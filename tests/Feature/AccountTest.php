<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\Account;
use Illuminate\Support\Str;

class AccountTest extends TestCase
{
    public function test_asserting_a_json_paths_value(): void
    {
        $user = User::factory(['document' => Str::random(11)])->create();
        Account::factory(['user_id' => $user->id])->create();
        $this->actingAs($user);
        $response = $this->getJson('/api/account');

        $response
            ->assertStatus(200)
            ->assertJsonPath('status', true);
    }
}
