<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Str;

class UserRegisterTest extends TestCase
{
    public function test_asserting_a_json_paths_value(): void
    {
        $payload = [
            'name' => fake()->name(),
            'email' => fake()->safeEmail(),
            'document' => Str::random(11),
            'password' => fake()->unique()->randomDigitNotNull(14, false)
        ];

        $response = $this->postJson('/api/auth/register', $payload);

        $response
            ->assertStatus(200)
            ->assertJsonPath('status', true);
    }
}
