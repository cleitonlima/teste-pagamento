<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\Account;
use Illuminate\Support\Str;

class TransactionCreateTest extends TestCase
{
    public function test_asserting_a_json_paths_value(): void
    {
        $user = User::factory(['document' => Str::random(11)])->create();
        Account::factory(['user_id' => $user->id, 'balance' => 2])->create();
        $userPayee = User::factory(['document' => Str::random(11)])->create();
        $account = Account::factory(['user_id' => $userPayee->id])->create();
        $this->actingAs($user);
        $payload = [
            'userPayee' => $account->id,
            'value' => 1,
        ];
        $response = $this->postJson('/api/transaction', $payload);

        $response
            ->assertStatus(200)
            ->assertJsonPath('status', true);
    }
}
