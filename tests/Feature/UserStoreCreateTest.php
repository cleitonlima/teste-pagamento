<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Support\Str;

class UserStoreCreateTest extends TestCase
{
    public function test_asserting_a_json_paths_value(): void
    {
        $user = User::factory(['document' => Str::random(11)])->create();
        $this->actingAs($user);

        $response = $this->postJson('/api/store');

        $response
            ->assertStatus(200)
            ->assertJsonPath('status', true);
    }
}
