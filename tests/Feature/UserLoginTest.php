<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Support\Str;

class UserLoginTest extends TestCase
{
    public function test_asserting_a_json_paths_value(): void
    {
        $user =  User::factory(['document' => Str::random(11), 'password' => '123456'])->create();
        $payload = [
            'email' => $user->email,
            'password' => '123456'
        ];

        $response = $this->postJson('/api/auth/login', $payload);
        $response
            ->assertStatus(200)
            ->assertJsonPath('status', true);
    }
}
