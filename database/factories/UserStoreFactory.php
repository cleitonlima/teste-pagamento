<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class UserStoreFactory extends Factory
{
    public function definition(): array
    {
        return [
            'user_id' => 1,
        ];
    }
}
