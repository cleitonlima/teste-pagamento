# Teste Pagamento

Este projeto é destinado a pôr em exercício aprendizados e conhecimento técnico simulando um processo de pagamento entre usuários.

  

## Breifing

### Objetivo
O principal objetivo deste projeto é criar um API em Laravel, que possibilita.

- Cadastro de usuários,
- Cadastro de lojas
- Criação de contas para usuários,
- Criação de transferência entre contas.


# Padrões adotados
Para criação da API foi adotado alguns padrões e conceitos para melhor legibilidade e manutenção do projeto, como:
SOLID
Design Pattern (Singleton, Facades, Dependency Injection)

# Tecnologias
- API desenvolvida em Laravel
- Banco de Dados em MYSQL
- Autenticação de usuários com Sanctum 
- Sistema de fila utilizando Queue Jobs do Laravel
- PHPUnit para testes

## Uso
Para rodar o projeto é necessário ter o **docker** configurador e um terminal **bash**. Após o clonar o repositório, deve ser acessada a pasta raiz do projeto e rodado o comando: `sh build.sh` no terminal. Feito isso, só aguardar o projeto rodar e executar todos os scripts necessários. Para rodar os testes de integração, acessar o container e executar o comando `php artisan test` .

Para consumir API, pode ser acessada: https://documenter.getpostman.com/view/5269889/2s946bDvXK
Caso ocorra algum problema na visualização da collection, a lista de rotas segue abaixo:

**USER:**

Login de usuário: **[POST]** `/api/auth/login`, payload: `{ "email": "{email}", "password": "{password}" }` , a chamada irá retornar um token que deve ser usado nas chamadas privadas.

Criar usuário: **[POST]** `/api/auth/register`, payload: `{ "name": "{name}", "email": "{email}", "document": "{document}", "password": "{password}" }`

**USERSTORE:**

Criar loja para usuário logado: **[POST]**[NEED-AUTH] `/api/store`.

Obtém dados de loja de usuário logado: **[GET]**[NEED-AUTH] `/api/store`.

**ACCOUNT:**

Criar conta para usuario: **[POST]**[NEED-AUTH] `/api/account`.

Obtém dados de conta de usuário logado: **[GET]**[NEED-AUTH] `/api/account`.

**TRANSACTION:**

Criar conta para usuario: **[POST]**[NEED-AUTH] `/api/transaction`, payload:  `{ "userPayee": "{accountId}", "value": "{value}" }`

Por fim, é isso.
Qualquer dúvida, ou sugestão. Enviar e-mail para: cleitonmaiconl@gmail.com
