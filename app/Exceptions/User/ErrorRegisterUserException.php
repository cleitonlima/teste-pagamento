<?php

namespace App\Exceptions\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Exception;

class ErrorRegisterUserException extends Exception
{
    public function __construct ($message = null)
    {
        $this->message = isset($message['error']) ? $message['error'] : null;
        $this->code = isset($message['code']) ? $message['code'] : 500;
        $this->render();
    }

    public function render()
    {
        return response()->json([
            'status' => false,
            'message' => "Error: Problem in create account for this client",
            'Error' => $this->message
        ], $this->code);
    }
}
