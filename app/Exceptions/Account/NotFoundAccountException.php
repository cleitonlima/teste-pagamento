<?php

namespace App\Exceptions\Account;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Exception;

class NotFoundAccountException extends Exception
{
    public function render()
    {
        return response()->json([
            'status' => false,
            'message' => "Not found Account for this client"
        ], 404);
    }
}
