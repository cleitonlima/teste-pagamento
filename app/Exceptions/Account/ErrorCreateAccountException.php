<?php

namespace App\Exceptions\Account;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Exception;

class ErrorCreateAccountException extends Exception
{
    public function render()
    {
        return response()->json([
            'status' => false,
            'message' => "Error: Problem in create account for this client"
        ], 500);
    }
}
