<?php

namespace App\Exceptions\Transaction;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Exception;

class ErrorCreateTransactionException extends Exception
{
    public function __construct ($message = null)
    {
        $this->message = isset($message['error']) ? $message['error'] : null;
        $this->code = isset($message['code']) ? $message['code'] : 500;
        $this->render();
    }
    public function render()
    {
        return response()->json([
            'status' => false,
            'message' => "Error: Problem in make the transaction  for this client",
            'Error' => $this->message
        ], $this->code);
    }
}
