<?php

namespace App\Exceptions\UserStore;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Exception;

class ErrorCreateStoreException extends Exception
{
    public function render()
    {
        return response()->json([
            'status' => false,
            'message' => "Error: Problem in create store for this client"
        ], 500);
    }
}
