<?php

namespace App\Exceptions\UserStore;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Exception;

class NotFoundStoreException extends Exception
{
    public function render()
    {
        return response()->json([
            'status' => false,
            'message' => "Not found Store for this client"
        ], 404);
    }
}
