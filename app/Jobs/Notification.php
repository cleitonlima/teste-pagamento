<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Http;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class Notification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function handle(): void
    {
        try {
            $message = 'Notificação enviada com sucesso';
            $response = Http::get('http://o4d9z.mocklab.io/notify');
            $response->json('message') == 'Success' ? true : false;
            Log::info($message);
        } catch (\Throwable $exception) {
            Log::error($exception->getMessage());
        }
    }
}
