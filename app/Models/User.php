<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = [
        'name',
        'email',
        'password',
        'document'
    ];


    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    public function isStore()
    {
        return $this->hasOne(UserStore::class);
    }

    public function account()
    {
        return $this->hasOne(Account::class);
    }

    public function scopeUpdateBalancePayer($query, $value)
    {
        return $this->account->update(['balance' => $this->account->balance - $value]);
    }

    public function scopeUpdateBalancePayee($query, $value)
    {
        return $this->account->update(['balance' => $this->account->balance + $value]);
    }
}
