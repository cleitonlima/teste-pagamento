<?php

namespace App\Http\Controllers\Store;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\UserStore;
use App\Services\UserStore\UserStoreService;
use App\Exceptions\UserStore\NotFoundStoreException;

class UserStoreController extends BaseController
{
    public function  __construct(UserStoreService $userStoreService)
    {
        $this->userStoreService = $userStoreService;
    }
    public function __invoke(Request $request)
    {
        try {
            $userStore = $this->userStoreService->getStore($request);
            if($userStore instanceof UserStore) {
                $userStore = $userStore->toArray();
                return response()->json([
                    'status' => true,
                    'data' => $userStore,
                ], 200);
            } else {
                throw new NotFoundStoreException();
            }

        } catch (NotFoundStoreException $exception) {
            return $exception->render();
        }
    }
}
