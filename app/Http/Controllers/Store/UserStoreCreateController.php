<?php

namespace App\Http\Controllers\Store;

use Illuminate\Routing\Controller as BaseController;
use App\Models\UserStore;
use Illuminate\Http\Request;
use App\Services\UserStore\UserStoreCreateService;
use App\Exceptions\UserStore\ErrorCreateStoreException;

class UserStoreCreateController extends BaseController
{
    public function  __construct(UserStoreCreateService $userStoreCreateService)
    {
        $this->userStoreCreateService = $userStoreCreateService;
    }

    public function __invoke(Request $request)
    {
        try {
            if($this->userStoreCreateService->create($request) instanceof UserStore) {
                return response()->json([
                    'status' => true,
                    'message' => 'Store Created Successfully',
                ], 200);
            } else {
                throw new ErrorCreateStoreException();
            }
        } catch (ErrorCreateStoreException $exception) {
            return $exception->render();
        }
    }
}
