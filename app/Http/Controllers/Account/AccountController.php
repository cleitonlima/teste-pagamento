<?php

namespace App\Http\Controllers\Account;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\Account;
use App\Services\Account\AccountService;
use App\Exceptions\Account\NotFoundAccountException;

class AccountController extends BaseController
{
    public function  __construct(AccountService $accountService)
    {
        $this->accountService = $accountService;
    }

    public function __invoke(Request $request)
    {
        try {
            $account = $this->accountService->getAccount($request);
            if($account instanceof Account) {
                $account = $account->toArray();
                return response()->json([
                    'status' => true,
                    'data' => $account,
                ], 200);
            } else {
                throw new NotfoundAccountException();
            }

        } catch (NotfoundAccountException $exception) {
            return $exception->render();
        }
    }
}
?>
