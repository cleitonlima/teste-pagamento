<?php

namespace App\Http\Controllers\Account;

use Illuminate\Routing\Controller as BaseController;
use App\Models\Account;
use Illuminate\Http\Request;
use App\Services\Account\AccountCreateService;
use App\Exceptions\Account\ErrorCreateAccountException;

class AccountCreateController extends BaseController
{
    public function  __construct(AccountCreateService $accountCreateService)
    {
        $this->accountCreateService = $accountCreateService;
    }

    public function __invoke(Request $request)
    {
        try {
            if($this->accountCreateService->create($request) instanceof Account) {
                return response()->json([
                    'status' => true,
                    'message' => 'Account Created Successfully',
                ], 200);
            } else {
                throw new ErrorCreateAccountException();
            }
        } catch (ErrorCreateAccountException $exception) {
            return $exception->render();
        }
    }
}
