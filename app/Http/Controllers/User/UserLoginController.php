<?php

namespace App\Http\Controllers\User;

use Illuminate\Routing\Controller as BaseController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Services\User\UserService;
use App\Exceptions\User\ErrorLoginUserException;
use Illuminate\Support\Facades\Validator;

class UserLoginController extends BaseController
{
    public function  __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function __invoke(Request $request)
    {
        try {
            $validateUser = Validator::make($request->all(), [
                'email' => 'required|email',
                'password' => 'required'
            ]);


            if(!Auth::attempt($request->only(['email', 'password']))){
                throw new ErrorLoginUserException([
                    'error' => 'Email & Password does not match with our record.',
                    'code' => 401
                ]);
            }

            if($validateUser->fails()){
                throw new ErrorLoginUserException(['error' => $validateUser->errors(), 'code' => 401]);
            }

            $user = $this->userService->getUser($request);

            if($user instanceof User) {
                return response()->json([
                    'status' => true,
                    'message' => 'User Logged In Successfully',
                    'token' => $user->createToken("API TOKEN")->plainTextToken
                ], 200);
            } else {
                throw new ErrorLoginUserException();
            }
        } catch (ErrorLoginUserException $exception) {
            return $exception->render();
        }
    }
}
?>
