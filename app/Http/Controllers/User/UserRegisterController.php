<?php

namespace App\Http\Controllers\User;

use Illuminate\Routing\Controller as BaseController;
use App\Models\User;
use Illuminate\Http\Request;
use App\Services\User\UserRegisterService;
use App\Exceptions\User\ErrorRegisterUserException;
use Illuminate\Support\Facades\Validator;

class UserRegisterController extends BaseController
{
    public function  __construct(UserRegisterService $userRegisterService)
    {
        $this->userRegisterService = $userRegisterService;
    }

    public function __invoke(Request $request)
    {
        try {
            $validateUser = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email',
                'document' => 'required|unique:users,document',
                'password' => 'required'
            ]);

            if($validateUser->fails()){
                throw new ErrorRegisterUserException(['error' => $validateUser->errors(), 'code' => 401]);
            }
            $user = $this->userRegisterService->register($request);
            if($user instanceof User) {
                return response()->json([
                    'status' => true,
                    'message' => 'User Created Successfully',
                    'token' => $user->createToken("API TOKEN")->plainTextToken
                ], 200);
            }
            throw new ErrorRegisterUserException();

        } catch (ErrorRegisterUserException $exception) {
            return $exception->render();
        }
    }
}
