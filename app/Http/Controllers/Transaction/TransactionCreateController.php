<?php

namespace App\Http\Controllers\Transaction;

use Illuminate\Routing\Controller as BaseController;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Services\Transaction\TransactionCreateService;
use App\Services\Transaction\TransactionVerifyBalanceService;
use App\Jobs\Notification;
use App\Exceptions\Transaction\ErrorCreateTransactionException;
use Illuminate\Support\Facades\Validator;

class TransactionCreateController extends BaseController
{
    public function  __construct(
        TransactionCreateService $transactionCreateService,
        TransactionVerifyBalanceService $transactionVerifyBalanceService,
    ) {
        $this->transactionCreateService = $transactionCreateService;
        $this->transactionVerifyBalanceService = $transactionVerifyBalanceService;
    }

    public function __invoke(Request $request)
    {
        try {
            $validate = Validator::make($request->all(), [
                'userPayee' => 'required',
                'value' => 'required'
            ]);
            if($validate->fails()){
                throw new ErrorCreateTransactionException(['errors' => $validate->errors(), 'code' => 401]);
            }

            $isStore = $request->user()->isStore;
            if($isStore) {
                throw new ErrorCreateTransactionException([
                    'error' => 'The client store cant transfer any value',
                    'code' => 401
                ]);
            }

            $hasBalance = $this->transactionVerifyBalanceService->verifyUserBalance($request);
            if(!$hasBalance) {
                throw new ErrorCreateTransactionException([
                    'error' => 'The client has no sufficiente funds',
                    'code' => 401
                ]);
            }

            if($this->transactionCreateService->create($request) instanceof Transaction) {
                dispatch(new Notification);
                return response()->json([
                    'status' => true,
                    'message' => 'Value Transfered Successfully',
                ], 200);
            }
            throw new ErrorCreateTransactionException();

        } catch (ErrorCreateTransactionException $exception) {
            return $exception->render();
        }
    }
}
