<?php
    namespace App\Services\Account;
    use Illuminate\Http\Request;

    interface AccountCreateInterface {
        function create(Request $request);
    }
