<?php
    namespace App\Services\Account;
    use Illuminate\Http\Request;

    interface AccountInterface {
        function getAccount(Request $request);
    }
