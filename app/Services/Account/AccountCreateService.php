<?php

namespace App\Services\Account;

use Illuminate\Http\Request;
use App\Models\Account;
use Illuminate\Support\Facades\Log;

class AccountCreateService implements AccountCreateInterface
{
    protected $defautValueAccount = 0;

    public function __construct(Account $account)
    {
        $this->account = $account;
    }
    public function create(Request $request)
    {
        try {
            return $this->account->updateOrCreate([
                'user_id' => $request->user()->id,
                'balance' =>  $this->defautValueAccount
            ]);
        } catch (\Throwable $exception) {
            Log::error($exception->getMessage());
            return null;
        }
    }
}
?>
