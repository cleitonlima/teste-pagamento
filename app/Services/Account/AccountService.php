<?php

namespace App\Services\Account;

use Illuminate\Http\Request;
use App\Models\Account;
use Illuminate\Support\Facades\Log;

class AccountService implements AccountInterface
{
    public function __construct(Account $account)
    {
        $this->account = $account;
    }
    public function getAccount(Request $request)
    {
        try {
            return $this->account->where('user_id', $request->user()->id)->first();
        } catch (\Throwable $exception) {
            Log::error($exception->getMessage());
            return null;
        }
    }
}
?>
