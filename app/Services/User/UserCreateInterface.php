<?php
    namespace App\Services\User;
    use Illuminate\Http\Request;

    interface UserCreateInterface {
        function register(Request $request);
    }
