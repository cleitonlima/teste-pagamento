<?php

namespace App\Services\User;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;

class UserRegisterService implements UserCreateInterface
{
    public function __construct(User $user)
    {
        $this->user = $user;
    }
    public function register(Request $request)
    {
        try {
            return $this->user->updateOrCreate([
                'name' => $request->name,
                'email' => $request->email,
                'document' => $request->document,
                'password' => Hash::make($request->password)
            ]);
        } catch (\Throwable $exception) {
            Log::error($exception->getMessage());
            return null;
        }
    }
}
?>
