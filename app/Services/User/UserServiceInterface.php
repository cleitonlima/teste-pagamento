<?php
    namespace App\Services\User;
    use Illuminate\Http\Request;

    interface UserServiceInterface {
        function getUser(Request $request);
    }
