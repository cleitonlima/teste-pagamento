<?php

namespace App\Services\User;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;

class UserService implements UserServiceInterface
{
    public function __construct(User $user)
    {
        $this->user = $user;
    }
    public function getUser(Request $request)
    {
        try {
            return $this->user->where('email', $request->email)->first();
        } catch (\Throwable $exception) {
            Log::error($exception->getMessage());
            return null;
        }
    }
}
?>

