<?php
    namespace App\Services\Transaction;
    use Illuminate\Http\Request;

    interface TransactionVerifyBalanceServiceInterface {
        function verifyUserBalance(Request $request);
    }
