<?php

namespace App\Services\Transaction;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Log;

class TransactionUpdateBalancePayeeService implements TransactionUpdateBalanceInterface
{
    public function __construct(User $user)
    {
        $this->user = $user;
    }
    public function updateBalance(Request $request)
    {
        try {
            return  $this->user
                    ->find($request->userPayee)
                    ->updateBalancePayee($request->value);
        } catch (\Throwable $exception) {
            Log::error($exception->getMessage());
            return null;
        }
    }
}
?>
