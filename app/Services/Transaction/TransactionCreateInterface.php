<?php
    namespace App\Services\Transaction;
    use Illuminate\Http\Request;

    interface TransactionCreateInterface {
        function create(Request $request);
    }
