<?php

namespace App\Services\Transaction;

use Illuminate\Http\Request;
use App\Models\Transaction;
use Illuminate\Support\Facades\Log;
use App\Services\Gateway;
use App\Services\Transaction\TransactionUpdateBalancePayerService;
use App\Services\Transaction\TransactionUpdateBalancePayeeService;
use App\Services\Gateway\GatewayService;

class TransactionCreateService implements TransactionCreateInterface
{

    public function __construct(
        Transaction $transaction,
        TransactionUpdateBalancePayerService $transactionUpdateBalancePayerService,
        TransactionUpdateBalancePayeeService $transactionUpdateBalancePayeeService,
        GatewayService $gatewayService
    ) {
        $this->transaction = $transaction;
        $this->transactionUpdateBalancePayerService = $transactionUpdateBalancePayerService;
        $this->transactionUpdateBalancePayeeService = $transactionUpdateBalancePayeeService;
        $this->gatewayService = $gatewayService;
    }

    public function create(Request $request)
    {
        try {
            \DB::beginTransaction();
            if($this->gatewayService->validate()){
                $transaction = $this->transaction->create([
                    'account_id_payer' => $request->user()->account->first()->toArray()['id'],
                    'account_id_payee' => $request->userPayee,
                    'value' => $request->value
                ]);
                if(!$this->transactionUpdateBalancePayerService->updateBalance($request)) {
                    throw new \Throwable();
                }
                if(!$this->transactionUpdateBalancePayeeService->updateBalance($request)) {
                    throw new \Throwable();
                }
            } else {
                throw new \Throwable();
            }
            \DB::commit();
            return $transaction;
        } catch (\Throwable $exception) {
            \DB::rollback();
            Log::error($exception->getMessage());
            return null;
        }
    }
}
?>
