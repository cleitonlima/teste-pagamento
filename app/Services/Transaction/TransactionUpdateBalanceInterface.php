<?php
    namespace App\Services\Transaction;
    use Illuminate\Http\Request;

    interface TransactionUpdateBalanceInterface {
        function updateBalance(Request $request);
    }
