<?php

namespace App\Services\Transaction;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class TransactionUpdateBalancePayerService implements TransactionUpdateBalanceInterface
{
    public function updateBalance(Request $request)
    {
        try {
            return  $request
                    ->user()
                    ->updateBalancePayer($request->value);
        } catch (\Throwable $exception) {
            Log::error($exception->getMessage());
            return null;
        }
    }
}
?>
