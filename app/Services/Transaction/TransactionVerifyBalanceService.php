<?php

namespace App\Services\Transaction;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class TransactionVerifyBalanceService implements TransactionVerifyBalanceServiceInterface
{
    public function verifyUserBalance(Request $request)
    {
        try {
            return $request->user()->account->balance >= $request->value;
        } catch (\Throwable $exception) {
            Log::error($exception->getMessage());
            return false;
        }
    }
}
?>
