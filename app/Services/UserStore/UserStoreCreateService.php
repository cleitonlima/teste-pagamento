<?php

namespace App\Services\UserStore;

use Illuminate\Http\Request;
use App\Models\UserStore;
use Illuminate\Support\Facades\Log;

class UserStoreCreateService implements UserStoreCreateInterface
{
    protected $defautValueUserStore = 0;

    public function __construct(UserStore $userStore)
    {
        $this->userStore = $userStore;
    }
    public function create(Request $request)
    {
        try {
            return $this->userStore->updateOrCreate([
                'user_id' => $request->user()->id
            ]);
        } catch (\Throwable $exception) {
            Log::error($exception->getMessage());
            return null;
        }
    }
}
?>
