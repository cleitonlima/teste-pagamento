<?php

namespace App\Services\Userstore;

use Illuminate\Http\Request;
use App\Models\UserStore;
use Illuminate\Support\Facades\Log;

class UserStoreService implements UserStoreInterface
{
    public function __construct(UserStore $userStore)
    {
        $this->userStore = $userStore;
    }
    public function getStore(Request $request)
    {
        try {
            return $this->userStore->where('user_id', $request->user()->id)->first();
        } catch (\Throwable $exception) {
            Log::error($exception->getMessage());
            return null;
        }
    }
}
?>
