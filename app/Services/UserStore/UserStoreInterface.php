<?php
    namespace App\Services\Userstore;
    use Illuminate\Http\Request;

    interface UserStoreInterface {
        function getstore(Request $request);
    }
