<?php
    namespace App\Services\Userstore;
    use Illuminate\Http\Request;

    interface UserStoreCreateInterface {
        function create(Request $request);
    }
