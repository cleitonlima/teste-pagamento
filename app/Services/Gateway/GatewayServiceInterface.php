<?php
    namespace App\Services\Gateway;
    use Illuminate\Http\Request;

    interface GatewayServiceInterface {
        function validate();
    }
