<?php

namespace App\Services\Gateway;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;

class GatewayService implements GatewayServiceInterface
{
    public function validate()
    {
        try {
            $response = Http::get('https://run.mocky.io/v3/8fafdd68-a090-496f-8c9a-3442cf30dae6');
            return $response->json('message') == 'Autorizado' ? true : false;

        } catch (\Throwable $exception) {
            Log::error($exception->getMessage());
            return null;
        }
    }
}
?>
