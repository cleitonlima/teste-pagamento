#### copy env
cp .env.example .env
#### build project
docker-compose build --no-cache
#### up project
docker-compose up -d
