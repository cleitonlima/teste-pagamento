<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\User\UserRegisterController;
use App\Http\Controllers\User\UserLoginController;
use App\Http\Controllers\Store\UserStoreController;
use App\Http\Controllers\Store\UserStoreCreateController;
use App\Http\Controllers\Account\AccountController;
use App\Http\Controllers\Account\AccountCreateController;
use App\Http\Controllers\Transaction\TransactionController;
use App\Http\Controllers\Transaction\TransactionCreateController;

$router->group(['prefix' => 'auth'], function ($router) {
    $router->post('/register', UserRegisterController::class);
    $router->post('/login',  UserLoginController::class);
});

Route::group(['middleware' => ['auth:sanctum']], function($router)
{
    $router->group(['prefix' => 'account'], function ($router)
    {
        $router->get('/', AccountController::class);
        $router->post('/', AccountCreateController::class);
    });

    $router->group(['prefix' => 'store'], function ($router)
    {
        $router->get('/', UserStoreController::class);
        $router->post('/', UserStoreCreateController::class);
    });

    $router->group(['prefix' => 'transaction'], function ($router)
    {
        $router->post('/', TransactionCreateController::class);
    });
});
